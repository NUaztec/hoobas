The hoobas CompositeObject
----------------------------------

The fundamental class is the :py:class:`hoobas.Composite.CompositeObject`.  While it provides useful iterators over
beads, topology and bonded force-field management, most of the magic is contained within two functionalities: deferred
evaluation and object introspection.

.. rubric:: Deferred function evaluation

In most cases, function evaluation needs to be deferred until the object is actually built. A system of polydisperse
polymer is one of the easiest examples to understand. If one wishes to have a simple polymer chain, where every
individual polymer has a different length between 10 and 15 and a side chain grafting density of 0.5,
this is easily accomplished by:

.. code-block:: python

    import hoobas
    import numpy as np
    polydisperse_KG = hoobas.LinearChain.GenericPolymer(n = lambda x : np.random.randint(10, 15))
    backbone.add_free_attachment_sites(key_search={}, properties={'orientation': np.array([1.0, 0.0, 0.0])}
    side_chain = hoobas.LinearChain.GenericPolymer(n = 2)
    sidechain.add_free_attachment_sites(key_search={'index': 0}, properties={'orientation': np.array([0., 0., -1.0])})
    polydisperse_KG.graft_external_object(sidechain, number = lambda number_monomers: int(number_monomers * 0.5))

When this chain is initialized, it stores the lambda function into its chain length. However, until it is actually built,
the side chain grafting cannot occur as the length is unknown. For this reason, the `hoobas.Composite.CompositeObject.graft_external_object`
function is decorated with `@hoobas.Composite.deferred` which stuffs the evaluation into a list to evaluate later. All
deferred calls are evaluated in the order they were called. All function arguments are kept in memory until evaluation.


.. rubric:: object introspection

Functions can be passed in most method arguments in `hoobas.Composite.CompositeObject`. These are able to query attributes
by using named arguments. For instance, in the polydisperse polymer above, the lambda uses an argument named number_monomers.
This in turn will be evaluated with the value contained in polydisperse.number_monomers. Under the hood, the method
`hoobas.Composite.CompositeObject.graft_external_object` takes the argument named number and checks its arguments by using
`hoobas.Composite.CompositeObject.resolve_args`, which can then be unpacked into number:

.. code-block:: python
    class CompositeObject:
        def graft_external_object(obj, n):
            if hasattr(n, '__call__'):
                n = n(**self.resolve_args(n))

Any new object based on the `hoobas.Composite.CompositeObject` class that implements new methods should handle functions
passed as arguments in this way, where possible.