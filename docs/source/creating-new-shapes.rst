New colloid shapes
--------------------------------------------

.. rubric:: Rigid bodies

Hoobas uses similar internal structure for rigid bodies as HOOMD-blue. Effectively, this means that inertia moments are defined
in the proper body frame (diagonal tensor) and an orientation is defined for every rigid body in the simulation. For objects to
be importable into HOOMD-blue, a method named `self.relative_positions` should be defined, as done for :py:class:`hoobas.Colloid.Colloid`.


Quaternions are used to represent orientation. For objects to be importable into HOOMD-blue, the orientation should also be written
to the first bead of the `CompositeObject`. Rotations should be handled by quaternion products, as done in :py:meth:`hoobas.Colloid.Colloid.rotate`.
Hoobas uses a body value to track whether a `CompositeObject` is rigid. For non-rigid bodies, this value is set to -1. A rigid
body may have non-rigid sections, and beads of those sections should have a -1 value for their body value.

.. rubric:: More shapes for Colloids

Colloid shapes are defined by :py:class:`hoobas.GenShape.Shape`. Geometric shapes, such as spheres, cubes, tetrahedrons
are considered `normalized` as they can simply be scaled by a size factor. These are opposed to complex shapes, which could
be, for instance, proteins. The prototype code for a simple geometric shape is

.. code-block:: python

    class GeometricShape(hoobas.GenShape.Shape):
        def __init__(self, Num):
            self.flags['normalized'] = True
            self.flags['simple_I_tensor'] = True
            self.flags['volume'] = V
            self.flags['surface'] = S
            self.table = np.array()

Where `self.table` is a 3 x N array defining a shape of volume `V` and area `S`. This is the `normalized` shape, i.e.,
what a size of 1.0 object should look like with approximately `Num` points on its surface. Very complicated and unique
shapes such as proteins will generally require a specialized implementation.














