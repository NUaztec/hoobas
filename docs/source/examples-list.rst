Tutorial and examples
------------------------

.. rubric:: Overview

.. toctree::
   :maxdepth: 3
   :caption: Examples of complex crystal systems

   example-dna-janus
   example-hexagonal-crystal-surface

.. toctree::
   :maxdepth: 3
   :caption: Examples of polydisperse polymers

   example-hyperbranched-polymer
   example-polyethylenimine
   example-random-ionomers
   example-rigid-bottlebrush

.. toctree::
   :maxdepth: 3
   :caption: Examples of lipid bilayers

   example-lipid-bilayers
