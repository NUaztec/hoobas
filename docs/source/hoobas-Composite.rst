Composite
------------------

.. rubric:: Overview

.. autosummary::
   :nosignatures:

   hoobas.Composite.deferred
   hoobas.Composite.CompositeObject
   hoobas.Composite.AttachmentSite
   hoobas.Composite.TopologyType
   hoobas.Composite.TopologyItem


.. rubric:: Details

.. automodule:: hoobas.Composite
    :synopsis: Complex objects in hoobas
    :members:
