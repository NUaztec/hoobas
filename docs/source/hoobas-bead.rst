Beads
--------------------
.. rubric:: Overview

.. autosummary::
   :nosignatures:

   hoobas.CGBead.Bead


.. rubric:: Details

.. automodule:: hoobas.CGBead
    :synopsis: Beads (atoms) in hoobas
    :members:
