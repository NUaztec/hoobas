Colloids
---------------
.. rubric:: Overview

.. autosummary::
   :nosignatures:

   hoobas.Colloid.Colloid
   hoobas.Colloid.SimpleColloid


.. rubric:: Details

.. automodule:: hoobas.Colloid
    :synopsis: Particles comprised of a rigid core and ligands
    :members:
