Simulation Domains
--------------------
.. rubric:: Overview

.. autosummary::
   :nosignatures:

   hoobas.SimulationDomain.Domain
   hoobas.SimulationDomain.Lattice
   hoobas.SimulationDomain.EmptyBox


.. rubric:: Details

.. automodule:: hoobas.SimulationDomain
    :synopsis: Defining a simulation domain in hoobas
    :members:
