Layers
--------------------
.. rubric:: Overview

.. autosummary::
   :nosignatures:

   hoobas.Layers.Multilayer
   hoobas.Layers.LayeredTiling


.. rubric:: Details

.. automodule:: hoobas.Layers
    :synopsis: 2D layers of :py:class:`hoobas.Composite.CompositeObject`
    :members:
