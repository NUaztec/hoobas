Linear chains
--------------------
.. rubric:: Overview

.. autosummary::
   :nosignatures:

   hoobas.LinearChain.LinearChain
   hoobas.LinearChain.GenericPolymer
   hoobas.LinearChain.GenericRingPolymer
   hoobas.LinearChain.GenericBlockRingPolymer
   hoobas.LinearChain.PolymerBySequence
   hoobas.LinearChain.DNAChain
   hoobas.LinearChain.DNA3SPNChain
   hoobas.LinearChain.RandomPolymer
   hoobas.LinearChain.PolyStyreneSulfonateChain

.. rubric:: Details

.. automodule:: hoobas.LinearChain
    :synopsis: Linear chains (polymers) in hoobas
    :members:
