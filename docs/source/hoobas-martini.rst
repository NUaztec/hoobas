Martini handlers
--------------------
.. rubric:: Overview

.. autosummary::
   :nosignatures:

   hoobas.MartiniModels.MartiniChain
   hoobas.MartiniModels.ITPMartiniParser
   hoobas.MartiniModels.MartiniForceMappings

.. rubric:: Details

.. automodule:: hoobas.MartiniModels
    :synopsis: Handling Martini types and nonbonded force-fields
    :members:
