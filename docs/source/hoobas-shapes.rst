Colloid shapes
--------------------
.. rubric:: Overview

.. autosummary::
   :nosignatures:

   hoobas.GenShape.shape


.. rubric:: Details

.. automodule:: hoobas.GenShape
    :synopsis: Shapes for :py:class:`hoobas.Colloid.Colloid`
    :members:
