.. hoobas documentation master file, created by
   sphinx-quickstart on Wed Apr 10 23:41:55 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to hoobas's documentation!
==================================

Hoobas is a Python package designed for building molecular topologies for molecular dynamics simulations. It is compatible for use with packages such as HOOMD-blue, EspresSo, and OpenMM.

.. toctree::
   :maxdepth: 3
   :caption: Introduction

   getting-started
   composite-objects
   examples-list

.. toctree::
   :maxdepth: 3
   :caption: Python Documentation

   hoobas

.. toctree::
   :maxdepth: 3
   :caption: Extending hoobas

   Understanding-composites
   creating-new-molecules
   creating-new-shapes

Hoobas is a highly object-oriented molecular builder. 
It enables to user to build complex, polydisperse topologies for molecular dynamics simulation. Hoobas can generate an initial system of colloids coated with arbitrary ligands and place them on either random positions or lattice sites defined by arbitrary lattice (supports triclinic boxes). Functions are built-in to add other particles, such as salts, and free chains floating.


Indices
******************

* :ref:`genindex`
* :ref:`modindex`

Source code
************

The hoobas git repository is on `bitbucket <https://bitbucket.org/NUaztec/hoobas>`_

hoobas overview
****************

This section will review basic features of hoobas. For detailed examples, please see the examples directory in the `hoobas git repository <https://bitbucket.org/NUaztec/hoobas>`_. If hoobas is installed or if the path to hoobas has been added to PYTHONPATH, these should work independently.

Here is where a basic overview of the code goes. For example:

The basic objects of hoobas are ``CompositeObjects``
