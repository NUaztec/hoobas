import numpy as np
import hoomd
import hoomd.md
import hoobas
"""
This file initializes a FCC crystal comprised of spherical colloids grafted with 2 kind of DNA sequences. The top half
of the sphere is coated with short DNA chains while the bottom part is coated with long DNA chains. Long DNA sticks to
short only. 

Strongly inspired by the following paper : Guolong Zhu, Ziyang Xu, Ye Yang, Xiaobin Dai, Li-Tang Yan, "Hierarchical 
crystals formed from DNA-functionalized Janus particles", ACS Nano 12, 2018
"""


# make some chain we want to graft
short_DNA_chain = hoobas.LinearChain.DNAChain(n_ss=1, n_ds=1, sticky_end=['L', 'M', 'N'])
long_DNA_chain = hoobas.LinearChain.DNAChain(n_ss=2, n_ds=5, sticky_end=['X', 'Y', 'Z'])

# add attachment sites on the first bead of each chain (hoobas will default 
# to grafting at index 0, but specify this to avoid a warning)
short_DNA_chain.add_free_attachment_sites(key_search={'index': 0})
long_DNA_chain.add_free_attachment_sites(key_search={'index': 0})

# make some colloids out of spheres with 20 points on the surface and size = 5, then graft them with chains
shape = hoobas.GenShape.Sphere(20)
colloid = hoobas.Colloid.SimpleColloid(shape=shape, size=5.0)
colloid.add_free_attachment_sites(key_search={'position': lambda p: p[2] > 0.0},
                                  properties={'orientation': lambda position: shape.surface_normal(position)})
colloid.graft_external_objects(short_DNA_chain, 20, connecting_topology='NP-S')
colloid.add_free_attachment_sites(key_search={'position': lambda p: p[2] < 0.0},
                                  properties={'orientation': lambda position: shape.surface_normal(position)})
colloid.graft_external_objects(long_DNA_chain, 20, connecting_topology='NP-S')


# create a FCC grid and add colloids on every lattice point
grid = hoobas.SimulationDomain.Lattice(lattice=35.0 * np.eye(3))
grid.add_particles_on_lattice([0., 0., 0.], colloid)
grid.add_particles_on_lattice([0.5, 0.5, 0.], colloid)
grid.add_particles_on_lattice([0.5, 0., 0.5], colloid)
grid.add_particles_on_lattice([0., 0.5, 0.5], colloid)
grid.cut_to_dimensions(bounds=[2, 2, 2])


# build this thing and give a random orientation to every colloid in there
builder = hoobas.Build.HOOMDBuilder(domain=grid)
builder.set_rotation_function('random')

# now to get hoomd to import this. Initialize a hoomd context
hoomd.context.initialize()

# make a snapshot and pass it to the builder
builder_box = builder.sys_box
hoomd_box = hoomd.data.boxdim(*builder_box)
snapshot = hoomd.data.make_snapshot(N=builder.num_beads, box=hoomd_box, particle_types=builder.bead_types)
builder.set_snapshot(snapshot)
system = hoomd.init.read_snapshot(snapshot)

# generate rigid body constraints for hoomd, this is provided by hoobas
cons = builder.aggregate_rigid_tuples()
hoomd_rigid = hoomd.md.constrain.rigid()
for c in cons:
    hoomd_rigid.set_param(c[0], c[1], c[2])
hoomd_rigid.create_bodies(create=False)

# Let's pass the bonded force-field to hoomd; for simple generic objects, we could have just defined it here in the
# first place, but others like DNA are predefined in their respective objects.
bond = hoomd.md.bond.harmonic()
for bond_type in builder.bond_types:
    bond.bond_coeff.set(bond_type.typename, k=bond_type['energy_constant'], r0=bond_type['distance_constant'])

angle = hoomd.md.angle.harmonic()
for angle_type in builder.angle_types:
    angle.angle_coeff.set(angle_type.typename, k=angle_type['energy_constant'], t0=angle_type['angle_constant'])

# Define the non-bonded force-field. This is a part where hoobas is not very efficient.
nlist = hoomd.md.nlist.cell()
radii = {'W': 5.0, 'P': 0.0, 'FL': 0.3, 'S': 0.5, 'A': 1.0, 'B': 0.5, 'X': 0.3,
         'Y': 0.3, 'Z': 0.3, 'L': 0.3, 'M': 0.3, 'N': 0.3}
sticky_ends = {''}
nonbonded = hoomd.md.pair.lj(r_cut=5.0 * 2.0**(1.0/6.0), nlist=nlist)
nonbonded.set_params(mode='shift')
for beadtypeA in builder.bead_types:
    for beadtypeB in builder.bead_types:
        lj_sigma = 0.5 * (radii[beadtypeA] + radii[beadtypeB])
        nonbonded.pair_coeff.set(beadtypeA, beadtypeB, epsilon=1.0, sigma=lj_sigma, r_cut=lj_sigma*2.0**(1.0 / 6.0))
nonbonded.pair_coeff.set('FL', 'FL', epsilon=1.0, sigma=0.4, r_cut=0.4 * 2**(1.0/6.0))
for beadtypeA in sticky_ends:
    for beadtypeB in sticky_ends:
        nonbonded.pair_coeff.set(beadtypeA, beadtypeB, epsilon=1.0, sigma=1.0, r_cut=2.0**(1.0/6.0))
    nonbonded.pair_coeff.set(beadtypeA, 'B', epsilon=1.0, sigma=0.6, r_cut=0.6*2.0**(1.0/6.0))
    nonbonded.pair_coeff.set(beadtypeA, 'FL', epsilon=1.0, sigma=0.43, r_cut=0.43*2.0**(1.0/6.0))
    nonbonded.pair_coeff.set(beadtypeA, 'A', epsilon=1.0, sigma=0.455, r_cut=0.455*2.0**(1.0/6.0))

nonbonded.pair_coeff.set('X', 'N', epsilon=7.0, sigma=1.0, r_cut=2.0)
nonbonded.pair_coeff.set('Y', 'M', epsilon=7.0, sigma=1.0, r_cut=2.0)
nonbonded.pair_coeff.set('Z', 'L', epsilon=7.0, sigma=1.0, r_cut=2.0)

group_all = hoomd.group.union(a=hoomd.group.nonrigid(), b=hoomd.group.rigid_center(), name='integrable')

# output a log and a trajectory
trajectory_dump = hoomd.dump.gsd(group=hoomd.group.all(), filename='JanusFCCtrajectory.gsd', period=1000, overwrite=True)
logger = hoomd.analyze.log(filename='log.log', period=10000, overwrite=True, quantities=['temperature',
                                                                                        'potential_energy',
                                                                                        'pressure'])
# and we're set, lets equilibrate this thing
hoomd.md.integrate.mode_standard(dt=0.003)
relaxation = hoomd.md.integrate.nve(group=group_all, limit=0.002)
hoomd.run(1000)
relaxation.disable()

langevin_integrator = hoomd.md.integrate.langevin(group=group_all, kT=1.5, seed=np.random.randint(65535, 4294967295))
hoomd.run(2e6)
langevin_integrator.disable()

npt_integrator = hoomd.md.integrate.npt(group=group_all, kT=1.5, P=1e-5, tau=0.85, tauP=2.0)
hoomd.run(2e7)

