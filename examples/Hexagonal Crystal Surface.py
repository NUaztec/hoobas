import hoobas
import hoomd
import hoomd.md
import math
import numpy as np

"""
This file initializes a hexagonal crystal comprised of large A particles on 1a and smaller B particles on 2d Wyckoff
positions. This simulations exposes the (111) plane comprised of A particles on both top and bottom layer in order
to calculate free energies. 

This generates a similar calculation to: S. E. Seo, M. Girard, M. Olvera de la Cruz, C. A. Mirkin "Non-equilibrium
anisotropic colloidal single crystal growth with DNA", Nat. Comm. 9, 2018
"""

# make DNA chains to graft
DNA_chain_A = hoobas.LinearChain.DNAChain(n_ss=2, n_ds=5, sticky_end=['X', 'Y', 'Z'])
DNA_chain_B = hoobas.LinearChain.DNAChain(n_ss=2, n_ds=5, sticky_end=['L', 'M', 'N'])

# define two spherical colloids
shape = hoobas.GenShape.Sphere(100)
colloidA = hoobas.Colloid.SimpleColloid(size=5.0, shape=shape, center_type='WA', surface_type='P')
colloidA.add_free_attachment_sites(key_search={'beadtype': 'P'},
                                   properties={'orientation': lambda position: shape.surface_normal(position)})
colloidB = hoobas.Colloid.SimpleColloid(size=2.5, shape=shape, center_type='WB', surface_type='P')
colloidB.add_free_attachment_sites(key_search={'beadtype': 'P'},
                                   properties={'orientation': lambda position: shape.surface_normal(position)})
# graft the chains
colloidA.graft_external_objects(DNA_chain_A, 100)
colloidB.graft_external_objects(DNA_chain_B, 50)

# define a hexagonal lattice
v1 = 20.0 * np.array([1., 0., 0.])
v2 = 20.0 * np.array([math.cos(math.radians(120.0)), math.sin(math.radians(120.0)), 0.0])
v3 = 20.0 * np.array([0., 0., 1.])
hexagonal_lattice = np.array([v1, v2, v3])

# add the particles to make an AlB2 type lattice
domain = hoobas.SimulationDomain.Lattice(lattice=hexagonal_lattice)
domain.add_particles_on_lattice([0., 0., 0.], colloidA)
domain.add_particles_on_lattice([1./3., 2./3., 0.5], colloidB)
domain.add_particles_on_lattice([2./3., 1./3., 0.5], colloidB)

domain.generate_crystal_slab(hkl=[1, 1, 1], inplane_basis=[[-1, 0, 1], [0, -1, 1]], bounds=[1, 1, 4], make_at=0)


# build this thing and give a random orientation to every colloid in there
builder = hoobas.Build.HOOMDBuilder(domain)
builder.set_rotation_function('random')

# now to get hoomd to import this. Initialize a hoomd context
hoomd.context.initialize()

# make a snapshot and pass it to the builder
builder_box = builder.sys_box
hoomd_box = hoomd.data.boxdim(*builder_box)
snapshot = hoomd.data.make_snapshot(N=builder.num_beads, box=hoomd_box, particle_types=builder.bead_types)
builder.set_snapshot(snapshot)
system = hoomd.init.read_snapshot(snapshot)

# generate rigid body constraints for hoomd, this is provided by hoobas
cons = builder.aggregate_rigid_tuples()
hoomd_rigid = hoomd.md.constrain.rigid()
for c in cons:
    hoomd_rigid.set_param(c[0], c[1], c[2])
hoomd_rigid.create_bodies(create=False)

# Let's pass the bonded force-field to hoomd; for simple generic objects, we could have just defined it here in the
# first place, but others like DNA are predefined in their respective objects.
bond = hoomd.md.bond.harmonic()
for bond_type in builder.bond_types:
    bond.bond_coeff.set(bond_type.typename, k=bond_type['energy_constant'], r0=bond_type['distance_constant'])

angle = hoomd.md.angle.harmonic()
for angle_type in builder.angle_types:
    angle.angle_coeff.set(angle_type.typename, k=angle_type['energy_constant'], t0=angle_type['angle_constant'])

# Define the non-bonded force-field. This is a part where hoobas is not very efficient.
nlist = hoomd.md.nlist.cell()
radii = {'WA': 5.0, 'WB': 2.5, 'P': 0.0, 'FL': 0.3, 'S': 0.5, 'A': 1.0, 'B': 0.5, 'X': 0.3,
         'Y': 0.3, 'Z': 0.3, 'L': 0.3, 'M': 0.3, 'N': 0.3}
sticky_ends = {''}
nonbonded = hoomd.md.pair.lj(r_cut=5.0 * 2.0**(1.0/6.0), nlist=nlist)
nonbonded.set_params(mode='shift')
for beadtypeA in builder.bead_types:
    for beadtypeB in builder.bead_types:
        lj_sigma = 0.5 * (radii[beadtypeA] + radii[beadtypeB])
        nonbonded.pair_coeff.set(beadtypeA, beadtypeB, epsilon=1.0, sigma=lj_sigma, r_cut=lj_sigma*2.0**(1.0 / 6.0))
nonbonded.pair_coeff.set('FL', 'FL', epsilon=1.0, sigma=0.4, r_cut=0.4 * 2**(1.0/6.0))
for beadtypeA in sticky_ends:
    for beadtypeB in sticky_ends:
        nonbonded.pair_coeff.set(beadtypeA, beadtypeB, epsilon=1.0, sigma=1.0, r_cut=2.0**(1.0/6.0))
    nonbonded.pair_coeff.set(beadtypeA, 'B', epsilon=1.0, sigma=0.6, r_cut=0.6*2.0**(1.0/6.0))
    nonbonded.pair_coeff.set(beadtypeA, 'FL', epsilon=1.0, sigma=0.43, r_cut=0.43*2.0**(1.0/6.0))
    nonbonded.pair_coeff.set(beadtypeA, 'A', epsilon=1.0, sigma=0.455, r_cut=0.455*2.0**(1.0/6.0))

nonbonded.pair_coeff.set('X', 'N', epsilon=7.0, sigma=1.0, r_cut=2.0)
nonbonded.pair_coeff.set('Y', 'M', epsilon=7.0, sigma=1.0, r_cut=2.0)
nonbonded.pair_coeff.set('Z', 'L', epsilon=7.0, sigma=1.0, r_cut=2.0)

group_all = hoomd.group.union(a=hoomd.group.nonrigid(), b=hoomd.group.rigid_center(), name='integrable')

# output a log and a trajectory
trajectory_dump = hoomd.dump.gsd(group=hoomd.group.all(), filename='HexagonalSurface.gsd', period=1000, overwrite=True)
logger = hoomd.analyze.log(filename='log.log', period=10000, overwrite=True, quantities=['temperature',
                                                                                        'potential_energy',
                                                                                        'pressure'])
# and we're set, lets equilibrate this thing
hoomd.md.integrate.mode_standard(dt=0.003)
relaxation = hoomd.md.integrate.nve(group=group_all, limit=0.002)
hoomd.run(1000)
relaxation.disable()

langevin_integrator = hoomd.md.integrate.langevin(group=group_all, kT=1.5, seed=np.random.randint(65535, 4294967295))
hoomd.run(2e6)
langevin_integrator.disable()



