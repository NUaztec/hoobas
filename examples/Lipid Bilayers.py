import numpy as np
import hoobas
import hoomd
import hoomd.md
import os
"""
This is an example of lipid bilayer membranes, with composition taken from H. J. Risselada, S. J. Marrink, "The molecular
face of lipid rafts in model membranes", PNAS 2008
"""

hoomd.context.initialize()
hoobas.Units.SimulationUnits.set_default('amu', 'nm', 'kJ/mol')
builder_box = hoobas.SimulationDomain.EmptyHexagonalPrism(24.0, c=10.0)

try:
    this_path = os.path.dirname(os.path.realpath(__file__))
except NameError:
    this_path = os.getcwd()

dipc_chain = hoobas.MartiniModels.ITPMartiniParser(os.path.join(this_path, 'lipids/martini_v2.0_DIPC_01.itp'),
                                                   os.path.join(this_path, 'lipids/DIPC-em.gro'))
dppc_chain = hoobas.MartiniModels.ITPMartiniParser(os.path.join(this_path, 'lipids/martini_v2.0_DPPC_01.itp'),
                                                   os.path.join(this_path, 'lipids/DPPC-em.gro'))
chol_chain = hoobas.MartiniModels.ITPMartiniParser(os.path.join(this_path, 'lipids/martini_v2.0_CHOL_01.itp'),
                                                   os.path.join(this_path, 'lipids/CHOL-em.gro'))

dipc_chain.set_residues('DIPC')
dppc_chain.set_residues('DPPC')
chol_chain.set_residues('CHOL')


ml = hoobas.Layers.LayeredTiling(NLayers=2, nx=1, ny=2)
ml.squish = 0.95
ml.add_species(dipc_chain, 540)
ml.add_species([dppc_chain, chol_chain], [828, 576], squish=[1.0, 0.6])

builder = hoobas.Build.HOOMDBuilder(builder_box)
builder.add_rho_molar_ions(55.0 / 4.0 * 0.9, qtype='P4', q=0.0, ion_mass=72.0, ion_diam=0.0)
builder.add_rho_molar_ions(55.0 / 4.0 * 0.1, qtype='BP4', q=0.0, ion_mass=72.0, ion_diam=0.0)
builder.set_attribute_by_beadtype('P4', 'residue', 'water')
builder.set_attribute_by_beadtype('BP4', 'residue', 'water')

builder.add_layer(ml, 0.0)
builder.permittivity = 15.0

builder.reduce_topology(bonds=True, angles=True, dihedrals=True)

sysbox = hoomd.data.boxdim(*builder.sys_box)
sn = hoomd.data.make_snapshot(builder.num_beads, sysbox, builder.bead_types)
builder.set_snapshot(sn)

builder.FileWriter.export_pdb('test.pdb')
system = hoomd.init.read_snapshot(sn)

bond = hoomd.md.bond.harmonic()

for bond_type in builder.bond_types:
    bond.bond_coeff.set(bond_type.typename, k=bond_type['energy_constant'], r0=bond_type['distance_constant'])
bond.bond_coeff.set('Excl', k=0.0, r0=0.0)

angle = hoomd.md.angle.cosinesq()
for angle_type in builder.angle_types:
    angle.angle_coeff.set(angle_type.typename, k=angle_type['energy_constant'], t0=angle_type['angle_constant'])

nlist = hoomd.md.nlist.tree()
nlist.reset_exclusions(['1-2'])
nonbonded = hoomd.md.pair.lj(nlist=nlist, r_cut=2.5 * 0.62)
nonbonded.set_params(mode="xplor")

def set_lj(m):
    hoomd.util.quiet_status()
    for beadtypeA in builder.bead_types:
        for beadtypeB in builder.bead_types:
            params = hoobas.MartiniModels.MartiniForceMappings.get_pair(beadtypeA, beadtypeB)
            nonbonded.pair_coeff.set(beadtypeA,
                                     beadtypeB,
                                     epsilon=params['eps'],
                                     sigma=m*params['sigma'],
                                     r_cut=2.5*params['sigma'])

    hoomd.util.unquiet_status()


group_all = hoomd.group.all()
bias_group = hoomd.group.tag_list('bias', [builder.p_num[-1]])

group_not_chol = hoomd.group.difference('notchol', a=group_all, b=hoomd.group.type(type='SP1'))
group_not_chol = hoomd.group.difference('notchol', a=group_not_chol, b=hoomd.group.type(type='SC3'))
group_not_chol = hoomd.group.difference('notchol', a=group_not_chol, b=hoomd.group.type(type='SC1'))

hoomd.dump.gsd(filename='test.gsd', period=2500, group=group_all, overwrite=True)
logger = hoomd.analyze.log(filename='test.log', period=25, quantities=['temperature',
                                                                         'potential_energy',
                                                                         'pressure',
                                                                         'volume',
                                                                         'bond_harmonic_energy',
                                                                         'pair_lj_energy'],
                           overwrite=True)

neq_steps = 20
neq_values = np.linspace(0.0, 1.0, neq_steps)
hoomd.md.update.zero_momentum(period=1)
nvei = hoomd.md.integrate.nve(group=group_all, limit=.002)
hoomd.md.integrate.mode_standard(dt=.001)
for step in range(neq_steps):
    set_lj(neq_values[step])
    print('setting LJ to :' + str(neq_values[step]))
    hoomd.run(1000)
nvei.disable()

hoomd.md.integrate.mode_standard(dt=.003)
logger.set_period(1000)
nvti = hoomd.md.integrate.langevin(group=group_all, kT=2.5, seed=np.random.randint(65535, 200000000))
hoomd.md.integrate.mode_standard(dt=.003)
hoomd.run(1e5)

# setup charges
charges = hoomd.md.charge.pppm(group=hoomd.group.charged(), nlist=nlist)
charges.set_params(Nx=128, Ny=128, Nz=128, order=6, rcut=1.2)
hoomd.run(1e5)
nvti.disable()

npti = hoomd.md.integrate.npt(group=group_all,
                              kT=2.5,
                              P=hoomd.variant.linear_interp([(0, logger.query('pressure')), (1e6, 0.0602)]),
                              tau=0.8,
                              tauP=2.0,
                              couple='xy')
hoomd.run(3e5, profile=True)

hoomd.md.integrate.mode_standard(dt=0.02)
npti.set_params(P=0.0602)
hoomd.run(4e6)
