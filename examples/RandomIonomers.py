import hoobas
import numpy as np
import hoomd
from hoomd import md
"""
this file sets up a simluation of ionomer melts at a charge fraction of 0.1 (global). Each monomer in the polymer has
a 10% change of being charged, in a uncorrelated fashion. This is strongly inspired by the following paper :
Boran Ma, Trung Dac Nguyen, Victor A. Pryamitsyn, Monica Olvera de la Cruz, "Ionic correlations in random ionomers",
ACS Nano 12 (2018).
"""


# set up an empty box 20 nm x 20 nm x 20 nm
units = hoobas.Units.SimulationUnits(length='nm', energy='kJ/mol', mass='amu')
domain = hoobas.SimulationDomain.EmptyCube(size=20.0, units=units)

# generate the random ionomers
charged_bead = hoobas.CGBead.Bead(position=np.array([0., 0., 0.]), beadtype='charged', charge=1.0)
non_charged_bead = hoobas.CGBead.Bead(position=np.array([0., 0., 0.]), beadtype='noncharged')

# generate the random polymer distributions
distribution_function = lambda: 'charged' if np.random.uniform() < 0.1 else 'noncharged'  # 10% of charged monomers
size_distribution = lambda: int(round(float(np.random.standard_normal(1))*4 + 8))+1  # chain length of 1 + 8 (+/- std of 4)

# generate chains
random_ionomers = hoobas.LinearChain.RandomPolymer(n_mono=size_distribution,
                                                   monomers={'charged': charged_bead, 'noncharged': non_charged_bead},
                                                   distribution=distribution_function)

# create a builder and add chains & counterions
builder = hoobas.Build.HOOMDBuilder(domain=domain)
builder.add_N_ext_obj(ext_obj=random_ionomers, N=40)
builder.fix_remaining_charge(ptype='Na', pion_mass=20.0, qp=1.0, ntype='Cl', nion_mass=20.0, qn=-1.0)
builder.permittivity = 15

# rename all bond types to backbone
for bond_type in builder.bond_types:
    builder.remap_bond(bond_type.typename, 'Backbone')  # remap every individual bond type in there to Backbone

hoomd.context.initialize()
sysbox = hoomd.data.boxdim(builder.sys_box[0], builder.sys_box[1], builder.sys_box[2],
                           builder.sys_box[3], builder.sys_box[4], builder.sys_box[5])

# set hoomd to builder
sn = hoomd.data.make_snapshot(N=builder.num_beads, box=sysbox, particle_types=builder.bead_types)
builder.set_snapshot(sn)
hoomd.init.read_snapshot(sn)

# force fields
bonded = hoomd.md.bond.harmonic()
bonded.bond_coeff.set('Backbone', k=1.0, r0=1.0)

nlist = hoomd.md.nlist.cell()
nonbonded = hoomd.md.pair.lj(nlist=nlist, r_cut=2.0**(1.0/6.0))
nonbonded.pair_coeff.set(['charged', 'noncharged', 'Na', 'Cl'],
                         ['charged', 'noncharged', 'Na', 'Cl'], epsilon=1.0, sigma=1.0)
nonbonded.set_params(mode='shift')

charge_forces = hoomd.md.charge.pppm(group=hoomd.group.charged(), nlist=nlist)
charge_forces.set_params(Nx=32, Ny=32, Nz=32, order=6, rcut=2.0**(1.0/6.0))

nve_integrator = hoomd.md.integrate.nve(group=hoomd.group.all(), limit=0.003)
hoomd.md.integrate.mode_standard(dt=0.003)
trajectory_writer = hoomd.dump.gsd(group=hoomd.group.all(), filename='./RandomIonomers.gsd', period=1000)
hoomd.run(1000)

nve_integrator.disable()

langevin_integrator = hoomd.md.integrate.langevin(kT=2.5,
                                                  group=hoomd.group.all(),
                                                  seed=np.random.randint(65535, 2000000))
hoomd.run(4e6)
