import hoobas
import hoomd
from hoomd import md
import numpy as np
import math

# this simulation file sets up 50 bottlebrush polymers where the backbone has 10 to 15 beads, half the beads are grafted
# to a flexible side chain of length 2 beads

# set up a simulation domain 30 sigma x 30 sigma x 30 sigma
domain = hoobas.SimulationDomain.EmptyCube(size=30.0)

# let's use a generic polymer as the backbone, of length 10-15 sigma, flat distribution
backbone = hoobas.LinearChain.GenericPolymer(n_mono=lambda: np.random.randint(10, 15), rigid=True, beadname='A')


def random_xy():  # define a function to set binding angles to the xy plane at random
    angle = np.random.uniform(0, 2*math.pi)
    return np.array([math.cos(angle), math.sin(angle), 0.0])


# the generic polymer is aligned on the z axis, so the grafting points should be in XY
backbone.add_free_attachment_sites(key_search={}, properties={'orientation': random_xy})

# let's use another generic polymer for the sidechain, of length 2 sigma, add an attachment site to the first bead
sidechain = hoobas.LinearChain.GenericPolymer(n_mono=2, rigid=False, beadname='B')
sidechain.add_free_attachment_sites(key_search={'index': 0}, properties={'orientation': np.array([0., 0., -1.0])})

# make the number of graft dependent on the number of monomers in the chain. This grafts on 50% of the monomers
backbone.graft_external_objects(external_object=sidechain,
                                number=lambda number_monomers: int(number_monomers*0.5),
                                connecting_topology=hoobas.Composite.BondType('A-B',
                                                                              topodict={'energy_constant': 1.0,
                                                                                        'distance_constant': 1.0},
                                                                              unitdict={'energy_constant': 'E/LL',
                                                                                        'distance_constant': 'L'}))

builder = hoobas.Build.HOOMDBuilder(domain)
builder.add_N_ext_obj(backbone, 50)

hoomd.context.initialize()
sysbox = hoomd.data.boxdim(builder.sys_box[0], builder.sys_box[1], builder.sys_box[2],
                           builder.sys_box[3], builder.sys_box[4], builder.sys_box[5])

# set hoomd to builder
sn = hoomd.data.make_snapshot(N=builder.num_beads, box=sysbox, particle_types=builder.bead_types)
builder.set_snapshot(sn)
hoomd.init.read_snapshot(sn)


# bonded force-field; the generic polymer creates a bond with spring constant 1 and length of kuhn length
bond = hoomd.md.bond.harmonic()
for bond_type in builder.bond_types:
    bond.bond_coeff.set(bond_type.typename, k=bond_type['energy_constant'], r0=bond_type['distance_constant'])

angle = hoomd.md.angle.harmonic()
for angle_type in builder.angle_types:
    angle.angle_coeff.set(angle_type.typename, k=angle_type['energy_constant'], t0=angle_type['angle_constant'])

# nonbonded force-field
nlist = hoomd.md.nlist.cell()
lj = hoomd.md.pair.lj(r_cut=2.0**(1.0/6.0), nlist=nlist)
lj.pair_coeff.set(['A', 'B'], ['A','B'], epsilon=1.0, sigma=1.0, r_cut=2.0**(1.0/6.0))

# write trajectory
hoomd.dump.gsd(filename='RigidBottlebrush.gsd', period=1000, overwrite=True, group=hoomd.group.all())

# energy minimization followed by langevin integration
hoomd.md.integrate.mode_standard(dt=0.003)
nve = hoomd.md.integrate.nve(group=hoomd.group.all(), limit=0.002)
hoomd.run(1000)
nve.disable()

langevin = hoomd.md.integrate.langevin(kT=1.0, group=hoomd.group.all(), seed=np.random.randint(65535, 2000000))
hoomd.run(5000)