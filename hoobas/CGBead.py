#coding: utf-8
import warnings
import numpy as np
from typing import Union
from hoobas.Quaternion import Quat


class Bead(object):
    """
    This is a particle (atom or CGbead)
    """
    ret_prop_list = ['mass', 'position', 'body', 'image', 'charge', 'diameter', 'orientation', 'moment_inertia']
    __slots__ = ['type', 'mass', '_position', 'body', 'residue', 'image', 'charge', 'diameter', 'orientation', 'moment_inertia']

    def __init__(self,
                 position: np.ndarray = np.array([0., 0., 0.]),
                 beadtype: str = 'notype',
                 mass: float = 1.0,
                 body: int = -1,
                 image: Union[None, np.ndarray] = None,
                 charge: float = 0.0,
                 diameter: float = 0.0,
                 quaternion: Union[None, np.ndarray, Quat] = None,
                 moment_inertia: Union[None, np.ndarray] = None,
                 residue: Union[None, str] = None):

        self.type = beadtype
        self.mass = mass
        self._position = np.array(position, dtype=np.single)
        self.body = body
        self.residue = residue
        if image is None:
            self.image = np.array([0, 0, 0], dtype=np.int32)
        else:
            self.image = np.array(image, dtype=np.int32)
        self.charge = charge
        self.diameter = diameter
        if quaternion is None:
            self.orientation = np.array([1.0, 0.0, 0.0, 0.0], dtype=np.single)
        else:
            try:
                self.orientation = quaternion.q_w_ijk
            except AttributeError:  # In case whatever is passed doesnt support q_w_ijk;
                warnings.warn(
                    'the quaternion passed doesnt support q_w_ijk; errors may be encountered while printing XML',
                    UserWarning)
                self.orientation = quaternion
        if moment_inertia is None:
            self.moment_inertia = np.array([0.0, 0.0, 0.0], dtype=np.single)
        else:
            self.moment_inertia = np.array(moment_inertia, dtype=np.single)

    @property
    def beadtype(self) -> str:
        return self.type

    @beadtype.setter
    def beadtype(self, val: str) -> None:
        self.type = str(val)

    @property
    def position(self) -> np.ndarray:
        return self._position

    @position.setter
    def position(self, value: np.ndarray) -> None:
        self._position[:] = value[:]

    # iterable for snapshot feed
    def __iter__(self):
        for prop in Bead.ret_prop_list:
            yield (prop, getattr(self, prop))

    def change_length_unit(self, multiplier: float) -> None:
        self.position *= multiplier
        self.diameter *= multiplier
        self.charge *= multiplier**0.5
        self.moment_inertia *= multiplier**2.0

    def change_mass_unit(self, multiplier: float) -> None:
        self.mass *= multiplier
        self.moment_inertia *= multiplier

    def change_energy_unit(self, multiplier: float) -> None:
        self.charge *= multiplier**0.5

    def change_units(self, m_length: float, m_energy: float, m_mass: float) -> None:
        self.change_mass_unit(m_mass)
        self.change_energy_unit(m_energy)
        self.change_length_unit(m_length)

    def __str__(self):
        return self.beadtype
