"""
    collection of methods / classes used by many other objects
"""

import numpy as np
from hoobas.Quaternion import Quat
import random
import math
import copy
from typing import Sequence, List


def get_rotation_matrix(cubic_plane: Sequence) -> np.ndarray:
    """Rotates a vector from the direction 001 to abc, where abc is a vector in cartesian coordinates (or cubic lattice)
    """
    plane_norm = np.array(cubic_plane)
    plane_norm /= np.linalg.norm(plane_norm)

    v = np.cross(np.array([0., 0., 1.]), plane_norm)

    if v[0] == 0.0 and v[1] == 0.0:  # surface is [001]
        return np.eye(3)
    else:
        cl = np.dot(plane_norm, np.array([0., 0., 1.]))
        mat_vx = np.array([[0.0, -v[2], v[1]],
                           [v[2], 0.0, -v[0]],
                           [-v[1], v[0], 0.0]])
        return np.eye(3) + mat_vx + (1.0 - cl) / np.linalg.norm(v) ** 2 * np.linalg.matrix_power(mat_vx, 2)


def get_inverse_rotation_matrix(cubic_plane: Sequence) -> np.ndarray:
    return np.linalg.inv(get_rotation_matrix(cubic_plane))


def gen_random_mat() -> np.ndarray:
    """
    generates a somewhat random rotation matrix
    :return: Rotation matrix
    """
    theta = random.uniform(0, 2*np.pi)
    z = random.uniform(0, 1)
    surface_norm = [(1-z**2)**0.5*np.cos(theta),
                    (1-z**2)**0.5*np.sin(theta),
                    z]
    return get_rotation_matrix(surface_norm)


def c_hoomd_box(v: Sequence, int_bounds: Sequence, z_multi: float = 1.00) -> List:
    """
    takes an arbitrary box defined by 3 full components vectors to hoomd standard box format.
    :param v: lattice vectors
    :param int_bounds: number of lattice periods in a, b, c
    :param z_multi: multiplier on the z direction; normally 1.0 but larger if surfaces are exposed
    :return: Box dimensions
    """
    vx = copy.copy(v[0][:])
    vy = copy.copy(v[1][:])
    vz = copy.copy(v[2][:])

    for i in range(len(vx)):
        vx[i] *= 2 * int_bounds[0]
        vy[i] *= 2 * int_bounds[1]
        vz[i] *= 2 * int_bounds[2] * z_multi

    lx = (np.dot(vx, vx)) ** 0.5
    a2x = np.dot(vx, vy) / lx
    ly = (np.dot(vy, vy) - a2x ** 2.0) ** 0.5
    xy = a2x / ly
    v0xv1 = np.cross(vx, vy)
    v0xv1mag = np.sqrt(np.dot(v0xv1, v0xv1))
    lz = np.dot(vz, v0xv1) / v0xv1mag
    a3x = np.dot(vx, vz) / lx
    xz = a3x / lz
    yz = (np.dot(vy, vz) - a2x * a3x) / (ly * lz)

    return [lx, ly, lz, xy, xz, yz]


def get_v1_v2_rotmat(initial: Sequence, final: Sequence) -> np.ndarray:
    """
    Calculates the rotation matrix that takes the vector initial to the vector final
    :param initial:
    :param final:
    :return:
    """
    vec_f = np.array(final, dtype=np.float64)
    vec_i = np.array(initial, dtype=np.float64)

    vec_f /= np.linalg.norm(vec_f)
    vec_i /= np.linalg.norm(vec_i)

    sine = np.cross(vec_f, vec_i)
    mat_vx = np.array([[0.0, -sine[2], sine[1]],
                       [sine[2], 0.0, -sine[0]],
                       [-sine[1], sine[0], 0.0]], dtype=np.float64)
    id3 = np.eye(3, 3, dtype=np.float64)
    cosine = np.dot(vec_i, vec_f)

    if abs(np.linalg.norm(sine)) < np.finfo(np.float64).eps * 10.0:
        if cosine > 0.0: # parallel vectors
            return id3
        else:  # opposite vectors, rotate by 180degrees
            perp = np.cross(vec_i, np.random.uniform(0, 1, 3))
            perp /= np.linalg.norm(perp)
            q = Quat([perp[0], perp[1], perp[2], 0.0])
            return q.transform

    return id3 + mat_vx + np.linalg.matrix_power(mat_vx, 2) / (1.0 + cosine)


def solve_diophantine(a, b, c):
    """
    diophantine equation solver, works, but doesn't guarantee nicest result to the equations
    Finds integer solutions to ax + by = c
    https://stackoverflow.com/questions/34324197/solving-linear-diophantine-equation?rq=1
    :param a:
    :param b:
    :param c:
    :return:
    """
    m1 = 1
    m2 = 0
    n1 = 0
    n2 = 1
    r1 = a
    r2 = b
    while r1 % r2 != 0:
        q = r1 / r2
        aux = r1 % r2
        r1 = r2
        r2 = aux
        aux3 = n1 - n2 * q
        aux2 = m1 - m2 * q
        m1 = m2
        n1 = n2
        m2 = aux2
        n2 = aux3
    return m2 * c, n2 * c


def find_next(input_list, current, value, prop=None):
    """
    Go through a sequence, and return the first index of value > current
    Specifying prop causes it look for the first element x of the list such that
    x[prop] == value

    :param input_list:
    :param current:
    :param value:
    :param prop:
    :return:
    """
    if current is None or value is None:
        return None

    if prop is None:
        for idx in range(current+1, len(input_list)):
            if input_list[idx] == value:
                return idx
    else:
        for idx in range(current+1, len(input_list)):
            if getattr(input_list[idx], prop) == value:
                return idx
    return None


def uniquetol(input, relative_tolerance=1e-5):
    # code lifted from divakar on stackoverflow :
    # https://stackoverflow.com/questions/37847053/uniquify-an-array-list-with-a-tolerance-in-python-uniquetol-equivalent
    # meant to mimic MATLAB uniquetol function

    tol = max(map(lambda x : abs(x), input)) * relative_tolerance
    return input[~(np.triu(np.abs(input[:, None] - input) <= tol, 1)).any(0)]


def find_nearest(array,value):
    # code lifted from unutbu on stackoverflow :
    # https://stackoverflow.com/questions/2566412/find-nearest-value-in-numpy-array
    idx = (np.abs(array-value)).argmin()
    return array[idx]


def bilaterate(P1, P2, r1, r2):
    # returns a point with distance r1 to P1 and r2 to P2
    diff = P2 - P1
    A = 2.0*diff[0]
    B = 2.0*diff[1]
    C = 2.0*diff[2]
    D = sum(P1**2.0) - sum(P2**2.0) + r2 * r2 - r1 * r1
    t = P1[0] * A + P1[1] * B + P1[2] * C + D
    t /= -A*diff[0] - B*diff[1] - C*diff[2]
    center_circle = P1 + t * diff
    d1 = np.linalg.norm(P1 - center_circle)
    if r1*r1 - d1*d1 < 0.0:
        raise ValueError('No intersection')
    rc1 = math.sqrt(r1*r1 - d1*d1)
    d2 = np.linalg.norm(P2 - center_circle)
    rc2 = math.sqrt(r2*r2 - d2*d2)
    crossv = np.cross(np.random.uniform(0, 1., 3), diff)
    crossv /= np.linalg.norm(crossv)
    return center_circle + rc1 * crossv


def trilaterate(P1, P2, P3, r1, r2, r3):
    # code taken from Andrew Wagner on stackoverflow:
    # https://stackoverflow.com/questions/1406375/finding-intersection-points-between-3-spheres
    temp1 = P2-P1
    e_x = temp1/np.linalg.norm(temp1)
    temp2 = P3-P1
    i = np.dot(e_x, temp2)
    temp3 = temp2 - i*e_x
    e_y = temp3/np.linalg.norm(temp3)
    e_z = np.cross(e_x,e_y)
    d = np.linalg.norm(P2-P1)
    j = np.dot(e_y, temp2)
    x = (r1*r1 - r2*r2 + d*d) / (2*d)
    y = (r1*r1 - r3*r3 -2*i*x + i*i + j*j) / (2*j)
    temp4 = r1*r1 - x*x - y*y
    if temp4 < 0:
        raise ValueError("The three spheres do not intersect!")
    z = math.sqrt(temp4)
    p_12_a = P1 + x*e_x + y*e_y + z*e_z
    p_12_b = P1 + x*e_x + y*e_y - z*e_z
    return p_12_a
