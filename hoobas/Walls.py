__author__ = 'martin'
import copy
import random
from math import *
from hoobas import Composite
import numpy as np

from hoobas import CGBead as CG
from hoobas.Util import get_rotation_matrix


class Wall(Composite.CompositeObject):

    def __init__(self, direction=None, bead_type=None, rigid=True, units=None):
        super(Wall, self).__init__(units)
        self.normal = direction
        if bead_type is None:
            self.Wall_bead_type = 'Wall'
        else:
            self.Wall_bead_type = bead_type
        
        self.body_num = -1
        if not rigid:
            self.transform_soft()
        else:
            self.body_num = [0]

    @property
    def body(self):
        return self._body

    @body.setter
    def body(self, val):
        self._body = val

    def transform_soft(self):
        pass


class PlaneWall(Wall):
    def __init__(self, normal = None, length = None, N = None, rigid = True, bead_type = None):
        if normal is None:
            Wall.__init__(self, direction=[0.0,0.0, 1.0], bead_type=bead_type, rigid=rigid)
        else:
            _dmpcpy = copy.deepcopy(normal)
            _dmpnorm = (_dmpcpy[0]**2 + _dmpcpy[1]**2 + _dmpcpy[2]**2)**0.5
            _dmpcpy[0] /= _dmpnorm
            _dmpcpy[1] /= _dmpnorm
            _dmpcpy[2] /= _dmpnorm
            Wall.__init__(self, direction=[_dmpcpy[0], _dmpcpy[1], _dmpcpy[2]], bead_type=bead_type, rigid=rigid)
            del _dmpnorm, _dmpcpy

        if length is None :
            self.edge_length = 1.0
        else:
            self.edge_length = length
        if N is None:
            self.N_edge = 25
        else:
            self.N_edge = N
        try:
            self.build_surf()
        except AssertionError:
            print('Unable to interpret input data')
        self.internal_c_position = [0.0, 0.0, 0.0]

        if abs(self.normal[0]) < 1e-5 and abs(self.normal[1]) < 1e-5 and abs(self.normal[2]>1e-5):
            pass
        else:
            self.rotate(self.normal)

    def transform_soft(self):
        # override since we want a rectangular set of springs and not the arbitrary one
        pass

    def build_surf(self):
        # create an N x N array that spans from - N L / 2 (N+1) to N L / 2 (N+1)

        # TODO : Fix the multiple isintance calls to a generic approach that wont be bothered by types

        if isinstance(self.edge_length, float) and isinstance(self.N_edge, float):
            x_tess_coordinates = np.linspace(- self.N_edge * self.edge_length / (2 * (self.N_edge + 1)),
                                             self.N_edge * self.edge_length / (2 * (self.N_edge + 1)),
                                             self.N_edge)
            y_tess_coordinates = np.linspace(- self.N_edge * self.edge_length / (2 * (self.N_edge + 1)),
                                             self.N_edge * self.edge_length / (2 * (self.N_edge + 1)),
                                             self.N_edge)
        elif isinstance(self.N_edge, float) and isinstance(self.edge_length, list):
            x_tess_coordinates = np.linspace(- self.N_edge * self.edge_length[0] / (2 * (self.N_edge + 1)),
                                             self.N_edge * self.edge_length[0] / (2 * (self.N_edge + 1)),
                                             self.N_edge)
            y_tess_coordinates = np.linspace(- self.N_edge * self.edge_length[1] / (2 * (self.N_edge + 1)),
                                             self.N_edge * self.edge_length[1] / (2 * (self.N_edge + 1)),
                                             self.N_edge)
        elif isinstance(self.edge_length, float) and isinstance(self.N_edge, list):
            x_tess_coordinates = np.linspace(- self.N_edge[0] * self.edge_length / (2 * (self.N_edge[0] + 1)),
                                             self.N_edge[0] * self.edge_length / (2 * (self.N_edge[0] + 1)),
                                             self.N_edge[0])
            y_tess_coordinates = np.linspace(- self.N_edge[1] * self.edge_length / (2 * (self.N_edge[1] + 1)),
                                             self.N_edge[1] * self.edge_length / (2 * (self.N_edge[1] + 1)),
                                             self.N_edge[1])
        else:
            assert(isinstance(self.edge_length, list) and isinstance(self.N_edge, list))
            x_tess_coordinates = np.linspace(- self.N_edge[0] * self.edge_length[0] / (2 * (self.N_edge[0] + 1)),
                                             self.N_edge[0] * self.edge_length[0] / (2 * (self.N_edge[0] + 1)),
                                             self.N_edge[0])
            y_tess_coordinates = np.linspace(- self.N_edge[1] * self.edge_length[1] / (2 * (self.N_edge[1] + 1)),
                                             self.N_edge[1] * self.edge_length[1] / (2 * (self.N_edge[1] + 1)),
                                             self.N_edge[1])
        xgrid, ygrid = np.meshgrid(x_tess_coordinates, y_tess_coordinates)

        for sidx_x in range(xgrid.shape[0]):
            for sidx_y in range(xgrid.shape[1]):
                self.beads.append(CG.Bead(position = np.array([xgrid[sidx_x][sidx_y] + self.internal_c_position[0],
                                                               ygrid[sidx_x][sidx_y] + self.internal_c_position[1],
                                                               self.internal_c_position[2]]),
                                          beadtype=self.Wall_bead_type, body=self.body_num))
                try:
                    self.pnum.append(self.pnum[-1])
                except IndexError:
                    self.pnum.append(0)

                self.positions = np.append(self.positions,[[xgrid[sidx_x][sidx_y] + self.internal_c_position[0],
                                                               ygrid[sidx_x][sidx_y] + self.internal_c_position[1],
                                                               self.internal_c_position[2]]], axis = 0 )
                self.att_sites.append(self.pnum[-1])
                self.rem_sites.append(self.pnum[-1])



