from setuptools import setup

with open('README.md') as readme_file:
    readme = readme_file.read()

setup(
    name='hoobas',
    version='2.0',
    description='A highly object-oriented molecular builder',
    long_description=readme,
    author='Martin Girard, Ali Ehlen, Anisha Shakya, Tristan Bereau, Monica Olvera de la Cruz',
    author_email='martin.girard@mpip-mainz.mpg.de',
    url='https://bitbucket.org/NUaztec/hoobas/',
    packages=['hoobas'],
    install_requires=['numpy', 'networkx', 'sphinx-autodoc-typehints'],
    license='GPL3'
    )
